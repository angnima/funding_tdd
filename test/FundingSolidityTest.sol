pragma solidity 0.8.10;

import "truffle/DeployedAddresses.sol";
import "truffle/Assert.sol";
import "../contracts/FundingContract.sol";

contract FundingSolidityTest {

    uint public initialBalance = 10 ether;
    uint public constant finney = 1e15;

    function testSettingAnOwnerDuringCreation() public {
        Funding funding = new Funding();
        Assert.equal(funding.owner(), address(this), "An owner is different than a deployer");
    }

    function testSettingAnOwnerOfDeployedContract() public {
        Funding funding = Funding(DeployedAddresses.Funding());
        Assert.equal(funding.owner(), msg.sender, "An owner is different than a deployer");
    }

    function testAcceptingDonations() public {
        Funding funding = new Funding();
        Assert.equal(funding.raised(), 0, "Initial raised amount is different than 0");
        funding.donate{value: 10 * finney}();
        funding.donate{value: 20 * finney}();
        Assert.equal(funding.raised(), 30 * 1e15, "Raised amount is different than sum of donations");
    }

    function testTrackingDonorsBalance() public {
        Funding funding = new Funding();
        funding.donate{value: 5 * finney}();
        funding.donate{value: 15 * finney}();
        Assert.equal(funding.balances(address(this)), 20 * finney, "Donator balance is different than sum of donations");
    }

}