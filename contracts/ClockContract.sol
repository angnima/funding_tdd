pragma solidity ^0.8.10;

contract Clock {
    uint private timestamp;

  function getNow() public view returns (uint) {
    if (timestamp > 0) {
      return timestamp;
    }
    return block.timestamp;
  }

  function setNow(uint _timestamp) public returns (uint) {
    timestamp = _timestamp;
    return timestamp;
  }
}