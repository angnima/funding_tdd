pragma solidity 0.8.10;

import "./ClockContract.sol";

contract Funding {

    uint public raised;
    address public owner;
    mapping(address => uint) public balances;

    constructor() {
        owner = msg.sender;
    }

    function donate() public payable {
        balances[msg.sender] += msg.value;
        raised += msg.value;
    }

}